#include "warehouse/warehouse.h"
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>
#include <string>

#include <cpprest/filestream.h>
#include <cpprest/http_client.h>
#include <nlohmann/json.hpp>

using namespace utility; // Common utilities like string conversions
using namespace web::http::client; // HTTP client features
using namespace concurrency::streams; // Asynchronous streams

TEST(SpringSchoolTests, JsonWorks) {
    nlohmann::json j;
    j["foo"] = "bar";

    auto expected = std::string(R"({"foo":"bar"})");
    auto js = j.dump();
    EXPECT_EQ(js, expected);
}

TEST(SpringSchoolTests, RestAPIWorks) {
    http_client client(
        "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com");
    web::uri_builder builder("/api/bot");

    auto http_return_code
        = client.request(web::http::methods::GET, builder.to_string())
              .then([=](web::http::http_response response) {
                  return response.status_code();
              });
    EXPECT_EQ(http_return_code.get(), 200);
}
