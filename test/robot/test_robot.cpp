#include <gtest/gtest.h>

#include "rest_api/i_rest_api.h"
#include "robot/robot.h"

#include <string>

struct MockRestApi : public IRestApi {
    web::json::value get(const std::string&) override {
        return web::json::value();
    };
    web::json::value put(const std::string&) override {
        return web::json::value();
    };
};
MockRestApi rest_api;
const std::vector<std::string> sessions {
    "alex", "betty", "carl", "charles", "cheryl", "clair", "cliff", "curtis"
};
const std::vector<std::string> directions { "north", "east", "south", "west" };

TEST(RobotTests, Move) {
    for (auto session : sessions) {
        Robot robot(rest_api, session);
        for (auto direction : directions) {
            EXPECT_EQ(robot.move(direction), true);
        }
    }
}

TEST(RobotTests, MoveWrongSessions) {
    std::string wrong_session { "wrongsession" };
    Robot robot(rest_api, wrong_session);
    for (auto direction : directions) {
        EXPECT_EQ(robot.move(direction), false);
    }
}

TEST(RobotTests, MoveWrongDirections) {
    std::string wrong_direction { "wrongdirection" };
    for (auto session : sessions) {
        Robot robot(rest_api, session);
        EXPECT_EQ(robot.move(wrong_direction), false);
    }
}