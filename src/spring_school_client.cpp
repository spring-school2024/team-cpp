#include <iostream>

#include "args.h"
#include "rest_api/rest_api.h"
#include "robot/robot.h"
#include "warehouse/warehouse.h"

void moveCommand(args::Subparser& parser) {
    args::Positional<std::string> session(parser, "session_id", "The session");
    args::Positional<std::string> direction(parser, "direction",
                                            "The direction.");
    parser.Parse();

    RestApi rest_api(
        U("http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/"));

    Robot robot(rest_api, args::get(session));
    robot.move(args::get(direction));
}

void capacityCommand(args::Subparser& parser) {
    parser.Parse();
    std::cout << "capacity return TODO" << std::endl;
}

int main(int argc, char** argv) {
    args::ArgumentParser parser("SpringSchool++");
    args::HelpFlag help(parser, "help", "Display this help menu",
                        { 'h', "help" });
    args::CompletionFlag completion(parser, { "complete" });

    args::Group commands(parser, "commands", args::Group::Validators::Xor);
    args::Command capacity(commands, "capacity",
                           "Get total warehouse capacity.", &capacityCommand);
    args::Command move(commands, "move", "Movement commands.", &moveCommand);

    try {
        parser.ParseCLI(argc, argv);
    } catch (const args::Completion& e) {
        std::cout << e.what();
        return 0;
    } catch (const args::Help&) {
        std::cout << parser;
        return 0;
    } catch (const args::ParseError& e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    } catch (const args::ValidationError& e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    return 0;
}