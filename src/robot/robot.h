#pragma once

#include "rest_api/i_rest_api.h"

#include <set>
#include <string>

class Robot {
public:
    Robot(IRestApi& rest, std::string& session_id);
    bool move(std::string& direction);

    std::tuple<size_t, size_t> position {};

private:
    static bool set_contains_string(const std::set<std::string>& set,
                                    const std::string& str);

    static const std::set<std::string> m_sessions;
    static const std::set<std::string> m_directions;

    IRestApi& m_rest_api;
    std::string& m_session_id;
};