#include "robot/robot.h"

const std::set<std::string> Robot::m_directions { "north", "east", "south",
                                                  "west" };
const std::set<std::string> Robot::m_sessions { "alex",    "betty",  "carl",
                                                "charles", "cheryl", "clair",
                                                "cliff",   "curtis" };

Robot::Robot(IRestApi& rest_api, std::string& session_id)
    : m_rest_api(rest_api)
    , m_session_id(session_id) { }

bool Robot::move(std::string& direction) {
    if (!set_contains_string(m_directions, direction)
        or !set_contains_string(m_sessions, m_session_id)) {
        return false;
    }
    m_rest_api.put("/api/bot/" + m_session_id + "/move/" + direction);
    return true;
}

bool Robot::set_contains_string(const std::set<std::string>& set,
                                const std::string& str) {
    if (set.contains(str)) {
        return true;
    }
    std::cout << "Invalid option \"" << str
              << "\". Valid options are.:" << std::endl;
    for (std::string string : set) {
        std::cout << string << " ";
    }
    std::cout << std::endl;
    return false;
}
