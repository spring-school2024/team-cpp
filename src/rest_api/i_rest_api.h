#pragma once

#include <string>
#include <cpprest/http_client.h>

class IRestApi{
public:
    virtual web::json::value get(const std::string& request) = 0;
    virtual web::json::value put(const std::string& request) = 0;
};
