#pragma once

#include "rest_api/i_rest_api.h"

#include <cpprest/filestream.h>
#include <cpprest/http_client.h>

class RestApi : public IRestApi {
public:
    RestApi(const std::string& serverUrl);
    web::json::value get(const std::string& request) override;
    web::json::value put(const std::string& request) override;

private:
    web::json::value makeRequest(const web::http::method& method,
                                 const std::string& request);
    const std::string serverUrl;
};