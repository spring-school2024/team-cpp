#include "rest_api/rest_api.h"

#include <cpprest/filestream.h>
#include <cpprest/http_client.h>
#include <iostream>

using namespace utility; // Common utilities like string conversions
using namespace web; // Common features like URIs.
using namespace web::http; // Common HTTP functionality
using namespace web::http::client; // HTTP client features
using namespace concurrency::streams;

RestApi::RestApi(const std::string& serverUrl)
    : serverUrl(serverUrl) { }

web::json::value RestApi::get(const std::string& request) {
    return makeRequest(methods::GET, request);
}

web::json::value RestApi::put(const std::string& request) {
    return makeRequest(methods::PUT, request);
}

web::json::value RestApi::makeRequest(const method& method,
                                      const std::string& request) {
    http_client client(serverUrl);
    auto http_response_json = client.request(method, request)
                                  .then([=](web::http::http_response response) {
                                      std::cout << "[" << response.status_code()
                                                << "] " << request << " -> "
                                                << response.extract_json().get()
                                                << std::endl;
                                      return response.extract_json();
                                  });
    return http_response_json.get();
}