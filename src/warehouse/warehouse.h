#pragma once

#include <string>
#include <tuple>
#include <vector>

struct Room {
    size_t storage_capacity {};
    bool has_top_wall;
    bool has_right_wall;
    bool has_bottom_wall;
    bool has_left_wall;
};

class Warehouse {
    std::vector<std::vector<Room>> layout;
};